# CV de Grégoire Humetz

> Website représentant mon curriculum vitae réalisé par moi même en Vuejs en CSS natif.
>
> Vous êtes libre de consulter le code source de ce projet à votre guise.

## Accès au Site

#### [Lien vers la version deployée](https://thefisto.gitlab.io/resumewebstite)

# Project setup
```batch
npm install
```

## Deployments
```batch
npm run serve
```
```batch
npm run build
```
